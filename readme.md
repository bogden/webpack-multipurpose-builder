Инструкция по работе с Frontend частью
--------------------------------------

Создание конфигурации для Webpack
=================================

```javascript

module.exports = require('webpack-multipurpose-builder')(entryPath, outputPath, options);

```

Дополнительные настройки 
=======================

 - *commonsChunk* - (boolean, default: true) Использовать ли оптимизацию групировки часто используемых пакетов от отдельный чанк с именем common.  
 - *alias* - (Object, default: <empty object>) Добавление индивидуальных алиасов.  
 - *provideJquery* - (boolean, default: false) Автоматическое подключение jQuery
 
Дополнительные действия
=======================

- `npm run debug:webpack-config`: Вывод в консоль параметров запуска webpack