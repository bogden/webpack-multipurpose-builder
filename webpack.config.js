require('colors');
const console = require('better-console');
const nodeExternals = require('webpack-node-externals')
const merge = require('webpack-merge');
const {upperCase} = require('lodash');
const packageConfig = require('./package');
const entries = require('./configs/entries');
const resolve = require('./configs/resolve');
const optimization = require('./configs/optimization');
const output = require('./configs/output');
const plugins = require('./configs/plugins');
const server = require('./configs/server');
const vueRule = require('./rules/vue');
const styleRule = require('./rules/style');
const jsRule = require('./rules/js');
const tsRule = require('./rules/ts');
const jsonRule = require('./rules/json');
const assetsRule = require('./rules/assets');

process.env.NODE_ENV = process.env.NODE_ENV ||
    (process.argv.indexOf('--development') !== -1 ?
        'development' :
        'production');
const NODE_ENV = process.env.NODE_ENV;

/**
 * Выводим некоторую информацию,
 * если имеется аргумент вывода подробностей сборки
 */
if (process.argv.indexOf('--verbose')) {
    console.log(`▶ ${upperCase(NODE_ENV)} MODE`.bold.green);
    console.log(('ℹ VERSION:' + packageConfig.version).blue);
}

/**
 * Настройка webpack
 * noinspection WebpackConfigHighlighting
 * @type {{mode: (*|string),entry: ({vendor: string[]}|entry), output: {path, filename, library}, optimization: ({splitChunks, minimizer}|optimization), resolve: {alias, extensions}, module: {rules: *[]}, devtool: string, devServer: (*|void), plugins: (*[]|plugins)}}
 */
module.exports = (workDirectory, outputDirectory, options = {}) => {
    options = Object.assign({
        commonsChunk: true,
        provideJquery: false,
        alias: {},
        entries: [],
        configuration: {},
        entriesConfig: {}
    }, options);

    return merge({
        mode: process.env.NODE_ENV,
        entry: entries(workDirectory, options.entriesConfig),
        output: output(outputDirectory),
        optimization: optimization(workDirectory, NODE_ENV),
        resolve: resolve(workDirectory, NODE_ENV, options.alias),
        module: {
            rules: [
                vueRule(),
                styleRule(NODE_ENV),
                jsRule(),
                assetsRule(),
                tsRule(),
                jsonRule()],
        },
        devtool: 'source-map',
        plugins: plugins(workDirectory, outputDirectory, options, NODE_ENV),
        devServer: server(outputDirectory),
        // externals: [nodeExternals()]
    }, options.configuration);
};
