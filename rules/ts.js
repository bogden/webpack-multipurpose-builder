const path = require('path');

module.exports = () => {
    return {
        test: /\.ts$/,
        loaders: [
            {
                loader: 'babel-loader',
                options: {
                    presets: [
                        [
                            '@babel/preset-env', {
                            targets: {
                                browsers: ['last 4 versions', 'IE 11'],
                            },
                        },
                        ]],
                    env: {
                        'staging': {
                            'presets': [
                                'minify',
                            ],
                        },
                        'production': {
                            'presets': [
                                'minify',
                            ],
                        },
                    },
                },
            },
            {
                loader: 'ts-loader',
                options: {
                    appendTsSuffixTo: [/\.vue$/],
                    configFile: path.resolve(__dirname, '../tsconfig.json'),
                },
            },
        ],
    };
};
