module.exports = () => {
    return {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
            loaders: {
                scss: 'vue-style-loader!css-loader!sass-loader',
                sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax', // <style lang="sass">
            },
            hotReload: true
        },
    };
};
