module.exports = () => {
    return {
        test: /\.(jpe?g|png|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
        use: 'base64-inline-loader?limit=100000&name=[name].[ext]',
    };
};
