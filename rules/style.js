const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = (mode = 'development') => {
    return {
        test: /\.(sa|sc|c)ss$/,
        oneOf: [
            // это соответствует `<style module>`
            {
                resourceQuery: /module/,
                use: [
                    'vue-style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            localIdentName: '[local]_[hash:base64:5]',
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            implementation: require("sass")
                        }
                    },
                ],
            },
            // это соответствует простому `<style>` или `<style scoped>`
            {
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            minimize: mode !== 'development',
                            importLoaders: 2,
                        },
                    },
                    // 'postcss-loader',
                    {
                        loader: 'sass-loader',
                        options: {
                            implementation: require("sass")
                        }
                    },
                ],
            },
        ],

    };
};
