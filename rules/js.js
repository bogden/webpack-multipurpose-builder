module.exports = () => {
    return {
        test: /\.js|es6/,
        exclude: /node_modules/,
        use: [
            {
                loader: 'babel-loader',
                options: {
                    presets: [
                        [
                            '@babel/preset-env', {
                            targets: {
                                browsers: ['last 3 versions'],
                            },
                        },
                        ]],
                    plugins: [
                        ["@babel/plugin-syntax-dynamic-import"],
                        ["@babel/plugin-proposal-decorators", {"legacy": true}]
                    ],
                },
            },
        ],
    };
};
