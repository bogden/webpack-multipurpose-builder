require('colors');
const console = require('better-console');
const treeify = require('treeify');
const config = require('./webpack.config')(__dirname, __dirname);

/**
 * Выводим в консоль финальные настроки webpack
 */
console.log('WEBPACK CONFIGURATION:'.green);
console.debug((treeify.asTree(config, true, true)).green);
