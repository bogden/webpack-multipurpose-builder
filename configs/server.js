const path = require('path');

module.exports = (folder) => {
    return {
        contentBase: path.resolve(folder),
        hot: true,
        inline: true,
        disableHostCheck: true,
        compress: true,
        port: 4100,
    };
};
