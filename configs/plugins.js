const path = require('path');
const fs = require('fs');
const treeify = require('treeify');
const {
    DefinePlugin,
    ProvidePlugin,
} = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const ImageminPlugin = require('imagemin-webpack-plugin').default;
const CopyWebpackPlugin = require('copy-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');

const packageConfig = require('../package');
const packageInfo = require(path.resolve(process.cwd(),'./package.json'));

/**
 * Получение настроект плагинов для webpack
 * @param sourceDirectory
 * @param outputDirectory
 * @param options
 * @param mode
 * @returns {*[]}
 */
module.exports = (sourceDirectory, outputDirectory, options = {}, mode = 'production') => {
    let plugins = [
        new DefinePlugin({
            BUILDER_MODE: '\'' + process.env.NODE_ENV + '\'',
            BUILDER_VERSION: '\'' + packageConfig.version.toString() + '\'',
            PACKAGE_VERSION: '\'' + packageInfo.version.toString() + '\'',
        }),
        new MiniCssExtractPlugin({
            filename: 'css/[name].css',
            chunkFilename: '[id].css',
        }),
        new CopyWebpackPlugin((() => {
            let copyFolders = [];
            if (fs.existsSync(path.join(sourceDirectory, 'html'))) {
                copyFolders.push({from: path.join(sourceDirectory, 'html'), to: outputDirectory});
            }
            if (fs.existsSync(path.join(sourceDirectory, 'img'))) {
                copyFolders.push({from: path.join(sourceDirectory, 'img'), to: 'img'});
            }
            if (fs.existsSync(path.join(sourceDirectory, 'fonts'))) {
                copyFolders.push({from: path.join(sourceDirectory, 'fonts'), to: 'fonts'});
            }
            if (fs.existsSync(path.join(sourceDirectory, 'content'))) {
                copyFolders.push({from: path.join(sourceDirectory, 'content'), to: 'content'});
            }
            if (fs.existsSync(path.join(sourceDirectory, 'manifest.json'))) {
                copyFolders.push({from: path.join(sourceDirectory, 'manifest.json'), to: outputDirectory});
            }
            treeify.asTree(copyFolders, true, true);
            return copyFolders;
        })(), {}),
        // new LodashModuleReplacementPlugin,
        new VueLoaderPlugin,
    ];

    if(options.hasOwnProperty('provideJquery') && options.provideJquery){
        plugins.push(new ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery',
            'window.jQuery': 'jquery',
        }));
    }

    if (mode === 'development') {
        //plugins.push(new HotModuleReplacementPlugin());
    }
    if (mode === 'production') {
        //plugins.push(new ImageminPlugin({test: /\.(jpe?g|png|gif|svg)$/i}));
    }
    return plugins;
};
