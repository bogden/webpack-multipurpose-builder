const path = require('path');
const fs = require('fs');
const console = require('better-console');

module.exports = (outputDirectory = null) => {
    console.info(('OUTPUT DIRECTORY: ' + outputDirectory).bold);
    return {
        path: outputDirectory,
        filename: 'js/[name].js',
        publicPath: '/assets/',
    };
};
