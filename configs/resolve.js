const path = require('path');

module.exports = (workDirectory = '', mode = 'production', alias) => {
    alias = Object.assign({
        'vue': mode === 'production' ? 'vue/dist/vue.min' : 'vue/dist/vue',
        '~environment': path.resolve(workDirectory, './js/environment'),
        '~components': path.resolve(workDirectory, './js/components'),
        '~mixins': path.resolve(workDirectory, './js/mixins'),
        '~modules': path.resolve(workDirectory, './js/modules'),
        '~api': path.resolve(workDirectory, './js/api'),
        '~plugins': path.resolve(workDirectory, './js/plugins'),
        '~router': path.resolve(workDirectory, './js/router'),
        '~store': path.resolve(workDirectory, './js/store'),
        '~source': path.resolve(workDirectory, './'),
        '#': path.resolve(workDirectory, '../../../'),
        '@scss': path.resolve(workDirectory, './scss'),
        '@svg': path.resolve(workDirectory, './svg'),
    }, alias);

    return {
        alias: alias,
        extensions: ['.es6', '.js', '.jsx', '.vue', '.ts'],
    };
};
