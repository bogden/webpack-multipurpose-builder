const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = (packageDirectory = '', options = {}, mode = 'production') => {
    let optimization = {
        splitChunks: {
            chunks: 'async',
            name: true,
            cacheGroups: {
                vendor: {
                    test: /node_modules(\/|\\\\).*\.js/,
                    chunks: 'initial',
                    name: 'vendor',
                    priority: 9,
                    enforce: true,
                }
            },
        },
        minimizer: [],
    };

    if(options.hasOwnProperty('commonsChunk') && options.commonsChunk){
        optimization.splitChunks.cacheGroups.commons = {
            test: /.*\.(js|es6)/,
                chunks: 'all',
                minChunks: 3,
                name: 'common',
                priority: 10,
                enforce: true,
        }
    }

    if (mode === 'production') {
        optimization.minimizer.push(
            new OptimizeCSSAssetsPlugin({})
        );
    }

    return optimization;
};
