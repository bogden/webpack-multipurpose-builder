const glob = require('glob');
const _ = require('lodash');
const console = require('better-console');
const colors = require('colors');
const path = require('path');

/**
 * Получаем списки entry для webpack
 * @param sourceDirectory
 * @param params
 * @returns {{vendor: string[]}}
 */
module.exports = (sourceDirectory = '', params = {}) => {
    let entry = {};
    let table = [
        {
            entry: ('vendor'.yellow),
            file: ('vendors libs'.yellow),
        },
    ];
    params = _.extend({
        entriesMask: './js/_entry/**/*.es6',
        renderStyles: true,
        entriesStyleMask: './scss/*.scss'
    }, params);
    /*
     * Добавляем js файлы в список entry
     * из папки js/_entry и вложеных в неё папкок.
     */
    glob.sync(path.resolve(sourceDirectory, params.entriesMask)).forEach((filePath) => {
        console.log(filePath);
        let name = _.snakeCase(
            (path.resolve(filePath).replace('\\', '/')).replace((path.resolve(sourceDirectory, './js/_entry/').replace('\\', '/')), '').replace(/\.(js|es6)$/, ''));
        table.push({
            entry: colors.cyan(
                name.substr(0, name.length <= 15 ? name.length : 15)),
            file: entry[name] = filePath,
        });
    });
    /*
     * Добавляем стили в список entry
     * Добавляются только файлы из папки src/scss,
     * файлы во вложеных папках игнорируются.
     */
    if(params.renderStyles){
        glob.sync(path.resolve(sourceDirectory, params.entriesStyleMask)).forEach((filePath) => {
            let name = _.snakeCase(
                (path.resolve(filePath).replace('\\', '/')).replace((path.resolve(sourceDirectory, './scss/').replace('\\', '/')), '').replace(/\.scss$/, ''));
            table.push({
                entry: colors.blue(
                    name.substr(0, name.length <= 15 ? name.length : 15) +
                    '...'), file: entry[name + '_style'] = filePath,
            });
        });
    }
    console.table(table);
    return entry;
};
